# Json2MetaJava : a meta transpiler
Provide a json sample, this project will reify a Java class model and a parser to use such data in any project.



# Discussion
* Quick and dirty implementation.
* Handle automatically list (including generics), arrays, int, double, String, boolean. 
* Provide an interactive mode where user is asked to confirm or enter names and types.


# TODO
* Create a kind of "update" mode to refine the model with several json sample.