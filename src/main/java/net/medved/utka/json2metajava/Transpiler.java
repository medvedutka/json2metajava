package net.medved.utka.json2metajava;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

public class Transpiler {

    private final Map<String, Map<String, String>> classes = new HashMap<>();
    private boolean generateGetter = true;
    private boolean generateSetter = true;
    private String rootClassName = "Root";

    private File outputFolder = null;
    private String json = null;
    private boolean quiet = false;
    private String userDefinedPackage = "";
    
    public void setRootClassName(String rcn) {
        this.rootClassName = rcn;
    }
    public void setJson(String json) {
        this.json = json;
    }
    public void setOutputFolder(File outputFolder) {
        this.outputFolder = outputFolder;
    }
    public void setQuiet(boolean quiet) {
        this.quiet = quiet;
    }
    public void setGenerateGetter(boolean generateGetter) {
        this.generateGetter = generateGetter;
    }
    public void setGenerateSetter(boolean generateSetter) {
        this.generateSetter = generateSetter;
    }
    public void setUserDefinedPackage(String userDefinedPackage) {
        this.userDefinedPackage = userDefinedPackage;
    }
    
    
    private PrintWriter getWritterForComponent(String name) throws Exception {
        if(outputFolder!=null) {
            File file = null;
            try {
                file = new File(outputFolder, name+".java");
                return new PrintWriter(file, "UTF-8");
            } catch (Exception ex) {
                throw new Exception("Cannot open file '"+ file.getAbsolutePath() +"' for writting",ex);
            } 
        }
        else {
            return new PrintWriter(System.out);
        }
    }

    public void transpile() throws Exception {
        
        JsonReader reader = Json.createReader(new StringReader(json));
        reiferAsClass(rootClassName, reader.read());
        
        for (Map.Entry<String, Map<String, String>> classe : classes.entrySet()) {
            String str = transpileAsJavaClass(classe.getKey(), classe.getValue()).toString();
            PrintWriter output = getWritterForComponent(classe.getKey());
            output.println(str);
            output.flush();
            output.close();
        }


        PrintWriter output = getWritterForComponent("Mapper");
        output.println("public class Mapper {\n");
        for (Map.Entry<String, Map<String, String>> classe : classes.entrySet()) {
            String str = generateMappers(classe.getKey(), classe.getValue());
            output.println(str);
        }
        output.println("}");
        output.flush();
        output.close();
        
    }
    
    private String reiferAsClass(String propName, JsonValue value) {
        JsonValue.ValueType valueType = value.getValueType();
        Map<String, String> suggestions = new HashMap<>();
        String typeName = null;
        String suggestedClass = null;
        info("Analyse de |" + propName + ":" + value.toString().replaceAll("\n", "") + "|");
        if (JsonValue.ValueType.OBJECT.equals(valueType)) {
            suggestedClass = propName.substring(0, 1).toUpperCase() + propName.substring(1);
            suggestions.put("o", "Object");
            typeName = question("Quel nom de classe pour l'objet '" + propName + "' ?", suggestions, true, suggestedClass);

            Map<String, String> ouiNon = new HashMap<>();
            ouiNon.put("o", "oui");
            ouiNon.put("n", "non");
            String reponse = question("Inspecter l'objet ?", ouiNon, false, "o");
            if ("o".equals(reponse)) {
                JsonObject o = (JsonObject) value;
                Map<String, String> newClass = new HashMap<>();
                classes.put(typeName, newClass);
                Set<String> properties = o.keySet();
                for (String prop : properties) {
                    JsonValue propValue = o.get(prop);
                    String propTypeName = reiferAsClass(prop, propValue);
                    newClass.put(prop, propTypeName);
                }
                info("Retour sur l'analyse de |" + propName + ":" + value.toString().replaceAll("\n", "") + "|");
            }
        } else {
            switch (valueType) {
                case NULL:
                    suggestedClass = propName.substring(0, 1).toUpperCase() + propName.substring(1);
                    break;
                case STRING:
                    suggestedClass = "String";
                    break;
                case NUMBER:
                    suggestedClass = "int";
                    suggestions.put("l", "long");
                    suggestions.put("f", "float");
                    suggestions.put("d", "double");
                    break;
                case TRUE:
                case FALSE:
                    suggestedClass = "boolean";
                    break;
                case ARRAY:
                    JsonArray arrayValue = (JsonArray) value;
                    String parametricType = null;
                    if (!arrayValue.isEmpty()) {
                        Map<String, String> ouiNon = new HashMap<>();
                        ouiNon.put("o", "oui");
                        ouiNon.put("n", "non");
                        String reponse = question("Inspecter la collection pour définir le type ?", suggestions, false, "o");
                        if ("o".equals(reponse)) {
                            JsonValue v0 = arrayValue.get(0);
                            String singularPropName = propName;
                            if (singularPropName.endsWith("s")) {
                                singularPropName = singularPropName.substring(0, singularPropName.length() - 1);
                            }
                            parametricType = reiferAsClass(singularPropName, v0);
                            info("Retour sur l'analyse de |" + propName + ":" + value.toString().replaceAll("\n", "") + "|");
                        }
                    }
                    if (parametricType != null) {
                        suggestions.put("a", parametricType + "[]");
                        if ("int".equals(parametricType)) {
                            parametricType = "Integer";
                        } else if ("long".equals(parametricType)) {
                            parametricType = "Long";
                        } else if ("float".equals(parametricType)) {
                            parametricType = "Float";
                        } else if ("double".equals(parametricType)) {
                            parametricType = "Double";
                        } else if ("boolean".equals(parametricType)) {
                            parametricType = "Boolean";
                        }
                        suggestedClass = "List<" + parametricType + ">";
                        suggestions.put("c", "Collection<" + parametricType + ">");
                    } else {
                        suggestedClass = "List";
                        suggestions.put("c", "Collection");
                        suggestions.put("a", "Object[]");
                    }
                    break;
            }
            typeName = question("Quel nom de classe pour la propriété '" + propName + "' ?", suggestions, true, suggestedClass);
        }
        return typeName;
    }

    private StringBuilder transpileAsJavaClass(String cName, Map<String, String> c) {
        StringBuilder result = new StringBuilder();
        //TODO: add user defined package
        if(userDefinedPackage!=null && !userDefinedPackage.isEmpty()) {
            result.append("package ").append(userDefinedPackage).append(";\n");
        }
        boolean importList = false;
        boolean importCollection = false;
        for (String fieldType : c.values()) {
            if("List".equals(fieldType) || fieldType.startsWith("List<")) {
                importList = true;
            }
            else if("Collection".equals(fieldType) || fieldType.startsWith("Collection<")) {
                importCollection = true;
            }
        }
        if(importList) {
            result.append("\nimport java.util.List;");
        }
        if(importCollection) {
            result.append("\nimport java.util.Collection;");    
        }
        result.append("\nimport java.io.Serializable;");
        result.append("\n");
        result.append("public class ").append(cName).append(" implements Serializable {\n");
        result.append("\n");
        result.append("\tpublic ").append(cName).append("() {\n");
        result.append("\t}\n");
        result.append("\n");
        for (Map.Entry<String, String> field : c.entrySet()) {
            result.append("\tprivate ").append(field.getValue()).append(" ").append(field.getKey()).append(";\n");
        }
        result.append("\n");

        for (Map.Entry<String, String> field : c.entrySet()) {
            if (generateGetter) {
                result.append("\tpublic ").append(field.getValue()).append(" get").append(upperCaseFirstLetter(field.getKey())).append("() {\n");
                result.append("\t\treturn ").append(field.getKey()).append(";\n");
                result.append("\t}\n");
            }
            if (generateSetter) {
                result.append("\tpublic void ").append(getSetterName(field.getKey())).append("(").append(field.getValue()).append(" ").append(field.getKey()).append(") {\n");
                result.append("\t\tthis.").append(field.getKey()).append(" = ").append(field.getKey()).append(";\n");
                result.append("\t}\n");
            }
            if (generateGetter || generateSetter) {
                result.append("\n");
            }
        }

        result.append("}\n");
        return result;
    }

    
    public String getSetterName(String prop) {
        return "set"+upperCaseFirstLetter(prop);
    }
    
    
    private String generateMappers(String cName, Map<String,String> c) {
        StringBuilder result = new StringBuilder();
        result.append("\tpublic static ").append(cName).append(" Map").append(cName).append("(JsonObject jo) {\n");
        result.append("\t\t").append(cName).append(" result = new ").append(cName).append("();\n");
 
        for(Map.Entry<String,String> entry : c.entrySet()) {
            String propType = entry.getValue();
            String propName = entry.getKey();
            if("int".equals(propType)||"Integer".equals(propType)) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(((JsonNumber)jo.get(\"").append(propName).append("\")).intValue());\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            }
            else if("long".equals(propType)||"Long".equals(propType)) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(((JsonNumber)jo.get(\"").append(propName).append("\")).longValue());\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            }           
            else if("double".equals(propType)||"Double".equals(propType)) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");                
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(((JsonNumber)jo.get(\"").append(propName).append("\")).doubleValue());\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            } 
            else if("float".equals(propType)||"Float".equals(propType)) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");                
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(((float)(JsonNumber)jo.get(\"").append(propName).append("\")).doubleValue());\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            }
            else if("String".equals(propType)) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");                
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(((JsonString)jo.get(\"").append(propName).append("\")).getString());\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            } 
            else if("boolean".equals(propType)||"Boolean".equals(propType)) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");                
                result.append("\t\t\t\tJsonValue jv = ").append("jo.get(\"").append(propName).append("\");\n");
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(JsonValue.ValueType.FALSE.equals(jv.getValueType()) ? false : true);\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            }
            else if(propType.endsWith("[]")) {
                String genericType = propType.substring(0,propType.length()-2);
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");                  
                result.append("\t\t\t\tJsonArray ja = (JsonArray)jo.get(\"").append(propName).append("\");\n");
                result.append("\t\t\t\t").append(genericType).append("[] value = new ").append(genericType).append("[ja.size()];\n");
                result.append("\t\t\t\tfor(int i = 0; i < ja.size(); i++) {\n");
                if(genericType.equals("int")||genericType.equals("Integer")) {
                    result.append("\t\t\t\t\tvalue[i] = ja.getInt(i);\n");
                }
                else if(genericType.equals("boolean")||genericType.equals("Boolean")) {
                    result.append("\t\t\t\t\tvalue[i] = ja.getBoolean(i);\n");
                }
                else if(genericType.equals("String")) {
                    result.append("\t\t\t\t\tvalue[i] = ja.getString(i);\n");
                }
                else {
                    result.append("TODO: ").append(propType).append(" ").append(propName).append("\n");
                }
                    
                result.append("\t\t\t\t\tvalue[i] = TODO;\n");
                result.append("\t\t\t\t}\n");
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(value);\n");
                result.append("\t\t\t}\n");
                result.append("\t\t}\n");
            }
            else if("List".equals(propType)||"Collection".equals(propType)) {
                result.append("TODO: ").append(propType).append(" ").append(propName).append("\n");
            }
            else if(propType.startsWith("List<")||propType.startsWith("Collection<")) {
                result.append("\t\t{\n");
                result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");
                result.append("\t\t\t\tJsonArray ja = (JsonArray)jo.get(\"").append(propName).append("\");\n");
                int iStart = propType.indexOf("<");
                int iEnd = propType.lastIndexOf(">");
                String genericType = propType.substring(iStart+1,iEnd);
                result.append("\t\t\t\tList<").append(genericType).append("> value = new ArrayList<>(ja.size());\n");
                result.append("\t\t\t\tfor(int i = 0; i < ja.size(); i++) {\n");
                if(genericType.equals("Integer")) {
                    result.append("\t\t\t\t\tvalue.add(ja.getInt(i));\n");
                }
                else if(genericType.equals("String")) {
                    result.append("\t\t\t\t\tvalue.add(ja.getString(i));\n");
                }
                else if(genericType.equals("Boolean")) {
                    result.append("\t\t\t\t\tvalue.add(ja.getBoolean(i));\n");
                }
                else if(classes.containsKey(genericType)) {
                    result.append("\t\t\t\t\tvalue.add(Map").append(genericType).append("(ja.getJsonObject(i)));\n");
                }
                else {
                    result.append("TODO: ").append(propType).append(" ").append(propName).append("\n");
                }
                result.append("\t\t\t\t}\n");
                result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(value);\n");
                result.append("\t\t\t}\n");                
                result.append("\t\t}\n");
            }
            else {
                if(classes.containsKey(propType)) {
                    result.append("\t\t{\n");
                    result.append("\t\t\tif(jo.containsKey(\"").append(propName).append("\") && !jo.isNull(\"").append(propName).append("\")) {\n");
                    result.append("\t\t\t\tresult.").append(getSetterName(propName)).append("(Map").append(propType).append("(jo.getJsonObject(\"").append(propName).append("\")));\n");
                    result.append("\t\t\t}\n");
                    result.append("\t\t}\n");
                }
                else {
                    result.append("TODO: ").append(propType).append(" ").append(propName).append("\n");
                }
            }

        }

        result.append("\t\treturn result;\n");
        result.append("\t}\n");
        return result.toString();
    }
    
    
    private void info(String s) {
        System.out.println(s);
    }

    private String question(String message, Map<String, String> suggestedAnswers, boolean allowFreeAnswer, String defaultAnswer) {
        if(quiet) {
            return defaultAnswer;
        }
        String result = "";
        StringBuilder messageLine = new StringBuilder(message);
        if (suggestedAnswers != null && !suggestedAnswers.isEmpty()) {
            messageLine.append(' ');
            Iterator<Map.Entry<String, String>> entryIter = suggestedAnswers.entrySet().iterator();
            while (entryIter.hasNext()) {
                Map.Entry<String, String> entry = entryIter.next();
                messageLine.append("[").append(entry.getKey()).append(']').append(entry.getValue());
                if (entryIter.hasNext()) {
                    messageLine.append(", ");
                }
            }
        }
        if (defaultAnswer != null) {
            messageLine.append(" (").append(defaultAnswer).append(')');
        }

        messageLine.append(" : ");

        System.out.println(messageLine.toString());
        Scanner input = new Scanner(System.in);
        while (true) {
            String userInput = input.nextLine();
            userInput = userInput.trim();
            if (suggestedAnswers != null && suggestedAnswers.containsKey(userInput)) {
                result = suggestedAnswers.get(userInput);
                break;
            } 
            else if (defaultAnswer != null && userInput.isEmpty()) {
                result = defaultAnswer;
                break;
            } 
            else if (allowFreeAnswer) {
                result = userInput;
                break;
            }
        }
        return result;
    }

    private String upperCaseFirstLetter(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }
}
