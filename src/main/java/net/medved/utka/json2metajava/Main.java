package net.medved.utka.json2metajava;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


public class Main {
    
    
    public static void main(String args[]) {
        Options options = new Options();
        
        {
            Option output = new Option("o", "output", true, "Output folder");
            output.setRequired(false);
            options.addOption(output);

            Option input = new Option("q", "quiet", false, "Does not ask any question and use always default or suggested options.");
            input.setRequired(false);
            options.addOption(input);

            Option rootClass = new Option("r", "root-class", true, "Class name of root (default is Root).");
            rootClass.setRequired(false);
            options.addOption(rootClass);
            
            Option noSetter = new Option("", "no-setter", false, "Does not generate setter.");
            noSetter.setRequired(false);
            options.addOption(noSetter);

            Option noGetter = new Option("", "no-getter", false, "Does not generate getter.");
            noGetter.setRequired(false);
            options.addOption(noGetter);
        
            Option userDefinedPackage = new Option("", "package", true, "Package name (default is: no package).");
            noGetter.setRequired(false);
            options.addOption(userDefinedPackage);
        }
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } 
        catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }

        String outputFilePath = cmd.getOptionValue("output", null);
        boolean quiet = cmd.hasOption("quiet");
        boolean noSetter = cmd.hasOption("no-setter");
        boolean noGetter = cmd.hasOption("no-getter");
        String rootClass = cmd.getOptionValue("root-class", "root");
        String userDefinedPackage = cmd.getOptionValue("package", "");
        
        //TODO: check if outputFilePath exists
        if(outputFilePath!=null&&!(new File(outputFilePath)).exists()) {
            System.out.println("Folder '" + outputFilePath + "' does not exist.");
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }
        
        Transpiler transpiler = new Transpiler();
        transpiler.setRootClassName(rootClass);
        transpiler.setGenerateGetter(!noGetter);
        transpiler.setGenerateSetter(!noSetter);
        transpiler.setUserDefinedPackage(userDefinedPackage);
        transpiler.setOutputFolder(new File(outputFilePath));
        transpiler.setJson(args[args.length-1]);
        try {
            transpiler.transpile();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
