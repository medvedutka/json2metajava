package net.medved.utka.json2metajava;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class Test1 {
    
    private static String EX1 = "{ \"name\":\"John\", \"age\":31, \"city\":\"New York\" }";
    private static String EX2 = "{\"activeFiltre\":true,\"motsCles\":\"\",\"fonctions\":[101833],\"lieux\":[712],\"pointGeolocDeReference\":{},\"secteursActivite\":[],\"typesContrat\":[],\"typesConvention\":[],\"niveauxExperience\":[],\"sorts\":[{\"type\":\"DATE\",\"direction\":\"DESCENDING\"}],\"pagination\":{\"startIndex\":20,\"range\":20},\"typeClient\":\"CADRE\"}";

    private static String EX4 = "{\"adresseOffre\":{\"adresseBatimentImmResidence\":\"LATITUDES ARBOIS BATIMENT A\",\"adresseCodePostal\":\"13857\",\"adresseNumeroEtVoie\":\"1060 RUE RENE DESCARTES\",\"adresseVille\":\"AIX EN PROVENCE\",\"idPays\":799,\"id\":126168721},\"audit\":{\"dateModification\":\"2017-05-05T12:47:30.000+0000\"},\"comptePersonnelIdNomOrganisation\":143850,\"datePublication\":\"2017-05-05T12:47:30.000+0000\",\"datePremierePublication\":\"2017-04-18T14:37:36.000+0000\",\"id\":162260373,\"idEtablissement\":563054,\"idInterlocuteurDirect\":56305401,\"idNomFonction\":101809,\"idNomFonctionInterlocuteur\":101830,\"idNomNiveauExperience\":597155,\"idNomSecteurActivite\":101569,\"idSecteurActiviteParent\":101753,\"idNomStatut\":101872,\"idNomTypeContrat\":101888,\"idNomTypePoste\":101900,\"idNomZoneDeplacement\":596720,\"idResponsable\":\"1000064\",\"intitule\":\"INGÉNIEUR ÉTUDES ET DÉVELOPPEMENT JAVA JEE F/H\",\"lieux\":[{\"idNomLieu\":573572,\"libelleLieu\":\"Montpellier - 34\"}],\"legacyLieux\":false,\"logoEtablissement\":\"media_entreprise/563054/logo_2S2I_SOLUTIONS_SERVICES_SUD_EST_563054.jpg\",\"nombrePostes\":1,\"nomCompteEtablissement\":\"2S2I SOLUTIONS & SERVICES SUD EST\",\"nomInterlocuteur\":\"SAVONAROLA\",\"numeroOffre\":\"162260373W\",\"offreConfidentielle\":false,\"offreQualifiee\":false,\"prenomInterlocuteur\":\"Anne\",\"referenceClientOffre\":\"AP/IEDI/MTP/170413\",\"salaireTexte\":\"A négocier\",\"tempsPartiel\":false,\"texteHtml\":\"<p>L'activité de notre division applicative se développe et nous cherchons à recruter un(e) Ingénieur développement Java/J2EE (H/F), pour renforcer une équipe de consultants intervenant sur un projet de développement d'applications de gestion s'intégrant dans un projet de rénovation globale d'un système d'informations. Nous sommes aussi amené à travailler pour des clients nationaux ou internationaux, principalement dans le secteur Finanace, banque ou Assurance mais aussi de secteurs d'activité variés tels que l'industrie, les télécoms, la géolocalisation, le e-commerce, les systèmes embarqués ou l'édition de logiciels.</p><p>Au sein de l'équipe existante et sous la responsabilité du chef de projet ou d'un(e) référent(e) technique, vous participez aux développements ou à l'évolution de logiciels en environnement Java/J2ee et êtes amené(e) à intervenir sur les phases suivantes :</p><ul><li>Analyse des besoins et conception logicielle</li><li>Rédaction des spécifications techniques</li><li>Développements ou paramétrage de solutions</li><li>Participer à la formation et au support utilisateurs</li><li>Maintenance et correction des applications pour préserver le niveau de service requis conformément aux spécifications fonctionnelles et techniques</li></ul><p>Enfin, vous êtes force de propositions auprès des différentes équipes en termes d’optimisation des performances et de veille technologique.</p>\",\"adresseUrlCandidature\":\"http://www.recrutement-2s2isolutions.com/emploi-ingenieur-etudes-et-developpement-java-jee_1097.html\",\"typeCandidature\":\"EMAIL_URL\",\"offreSalon\":false,\"latitude\":43.6152239,\"longitude\":3.8780617,\"geolocalisable\":true,\"texteHtmlProfil\":\"<p>Vous justifiez d'au moins 5 ans d’expérience sur un poste similaire en environnement technique Java/J2EE, et vous maîtrisez, idéalement, une (ou plusieurs) des technologies suivantes : Spring, Hibernate, GWT, Liferay, Scala, Flex, IPhone, Android, Maven, Jenkins, Wicket, Spring MVC, BIRT, Groovy, Tomcat…</p><p>Vous êtes organisé(e), autonome, rigoureux(se) et savez être créatif(ve). Vous êtes aussi reconnu(e) pour avoir de bonnes capacités rédactionnelles et des qualités humaines qui assurent votre bonne intégration et un sens aigu du service.</p><p>Vous cherchez à relever de nouveaux challenges, vous partagez nos valeurs et cherchez une entreprise qui saura reconnaître la vôtre, contactez-nous sans plus attendre et rencontrons-nous pour une future collaboration!</p>\",\"texteHtmlEntreprise\":\"<p>Depuis sa création, 2S 2I Solutions & Services a connu un fort développement et propose aujourd'hui des solutions, sur toute la France, en mode projet ou régie couvrant l’ensemble du cycle de vie du système d’information avec des intervenants qualifiés sur tous les métiers de l’ingénierie informatique et du consulting IT (ingénierie applicative, infrastructure & production, informatique industrielle ou conseil en SI et MOA).</p><p>Nos valeurs humaines sont au centre de notre modèle et nous agissons côte à côte en cultivant des valeurs communes d'Excellence, Intégrité, Créativité et Engagement partagées et portées par nos consultants à travers leurs projets. En intégrant 2S 2I Solutions & Services, vous rejoignez une équipe avec une vision différente de la SSII, un management de proximité et des opportunités de carrière fidèles à des objectifs fixés en commun.</p><p>Nous recherchons de nouveaux collaborateurs pour nous accompagner dans notre croissance et nos projets. Pourquoi pas vous!... </p>\",\"enseigne\":\"2S2I SOLUTIONS & SERVICES\",\"affichageLogo\":true}\n";
    private static String EX3 = "{\"p1\":null}";
    
    public Test1() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    
    /*@Test
    public void test3() throws Exception {
        JsonReader reader = Json.createReader(new StringReader(EX4));
        JsonObject jo = reader.readObject();
        if(!jo.isNull("pointGeolocDeReference")) {
            //result.setPointGeolocDeReference(MapPointGeolocDeReference(jo.getJsonObject("pointGeolocDeReference")));
        }
        
    }*/
    /*@Test
    public void test2() throws Exception {
        JsonReader reader = Json.createReader(new StringReader(EX3));
        JsonObject jo = reader.readObject();
        if(jo.isNull("p1")) {
            System.out.println("p1 is null");
        }
        ((JsonNumber)jo).doubleValue();
        JsonValue p1jv = jo.get("p1");
        //JsonObject p1jo = jo.getJsonObject("p1");
        JsonArray p1ja = jo.getJsonArray("p1");
    }*/
    
    
    @Test
    public void test() throws Exception {
        Transpiler transpiler = new Transpiler();
        transpiler.setJson(EX4);
        transpiler.setQuiet(true);
        transpiler.transpile();
    }
}
